# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.7] - 2024-11-26

### Added

### Fixed

- Fixed https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoadmin/-/issues/1

### Changed

### Removed


## [UNRELEASED] - 2024-xx-xx

### Added

- Added option to add extra labels to a fridge, which can be used in queries within Prometheus/Grafana
- Added filtering on, and displaying of, organization for `cryo_admin.apiv1.models.AlertUpdateResponse` for admin accounts

### Fixed

- Fixed filtering `cryo_admin.cryo_gram.models.FridgeChatUser` on organization causing crash of the application
- Fixed filtering `cryo_admin.cryo_gram.models.FridgeChat` on organization causing crash of the application
- Fixed filtering `cryo_admin.apiv1.models.ApiKey` on organization causing crash of the application

### Changed

- Changed AlertRule `alert_name` to be unique per `organization`

### Removed

## [1.0.6] - 2024-06-24

### Added

### Fixed

- Fixed a mistake was made during uploading the container causing it to not include the changes of 1.0.4

### Changed

### Removed

## [1.0.4] - 2024-06-21

### Added

- Added filter on organization for `cryo_admin.fridges.models.Fridge`

### Fixed

- Fixed timeout issues due to <https://github.com/jazzband/django-debug-toolbar/issues/1927> by pinning django-debug-toolbar to 4.3.0
- Fixed possiblity that an organization admin could add users to not their own organization and with higher permissions than their account has
- Fixed case sensitivity of fridge and organization names when trying to subscribe to a fridge chat via CryoGram

### Changed

- Added organization information to string version of `cryo_admin.alerts.models.AlertRule`. Used by admin to differentiate between identically named rules in different organizations

### Removed

## [1.0.3] - 2024-06-13

### Added

- Added flag to `cryo_admin.fridges.models.Fridge` to not participate in Service Discovery for Prometheus
- Added flag to `cryo_admin.fridges.models.Fridge` to not participate in alert updates
- Added flag to `cryo_admin.fridges.models.Fridge` to not participate in CryoGram requests

### Fixed

### Changed

- The service discovery endpoint (`/api/v1/sd/`) will only return a list of fridges with the `Fridge.enable_service_discovery` flag set to `True`
- The alert update endpoint (`/api/v1/alerts/`) will only return alert rules for fridges with the `Fridge.enable_alert_updates` flag set to `True`
- The CryoGram endpoint for listing the known fridges (`/api/v1/known_fridges/`) will only return fridges with the `Fridge.enable_cryogram_request` flag set to `True`
- The CryoGram endpoint for the chat_id of a fridge (`/api/v1/chat_id/`) will only return the chat_id when the `Fridge.enable_cryogram_request` flag set to `True`

### Removed

## [1.0.2] - 2024-06-11

### Added

- Added option to filter `AlertRule` based on organization in the admin interface

### Fixed

- Fixed returning the correct `host` format for prometheus service discovery

### Changed

- Changed `AlertRule` admin interface to include information about which organization the `AlertRule` belongs

## [1.0.1] - 2024-06-11

### Changed

- Changed `admin.site.site_header`
- Changed `admin.site.index_title`
- Changed `admin.site.site_title`
- Changed `unit` field of `AlertRule` to be nullable

## [1.0.0] - 2024-06-11

### Added

- Initial release of CryoAdmin

### Fixed

### Changed

### Removed
