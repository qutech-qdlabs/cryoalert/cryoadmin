from django.conf import settings
from django.db import models


class FridgeChatUserManager(models.Manager):
    """Manager for the FridgeChatUser model"""


class FridgeChatUser(models.Model):
    """Model representing a FridgeChat user"""

    # Data
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    username = models.CharField(max_length=200)

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)

    # Manager
    objects = FridgeChatUserManager()

    class Meta: ...

    def __str__(self) -> str:
        return f"ChatUser(name={self.username}, user={self.user})"
