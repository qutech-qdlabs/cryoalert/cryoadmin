from django.db import models


class FridgeChatManager(models.Manager):
    """Manager for the FridgeChat Model"""


class FridgeChat(models.Model):
    """Model representing a FridgeChat"""

    # Data
    fridge = models.OneToOneField("fridges.Fridge", on_delete=models.CASCADE)
    chat_id = models.BigIntegerField()

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)

    # Manager
    objects = FridgeChatManager()

    class Meta: ...

    def __str__(self) -> str:
        return f"FridgeChat for {self.fridge}"
