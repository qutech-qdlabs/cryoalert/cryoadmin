from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db import models
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms import NumberInput
from django.forms.models import ModelChoiceField
from django.http import HttpRequest

from cryo_admin.cryo_gram.models import FridgeChat
from cryo_admin.fridges.models import Fridge


@admin.register(FridgeChat)
class FridgeChatAdmin(admin.ModelAdmin):
    """FridgeChat Admin"""

    formfield_overrides = {
        models.IntegerField: {"widget": NumberInput(attrs={"width": "20"})},
    }

    @admin.display(description="Organization")
    def organization(self, obj):
        return obj.fridge.relay.organization

    @admin.display(description="Fridge Name")
    def fridge_name(self, obj):
        return obj.fridge.name

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["fridge_name", "chat_id", "organization"]
        return ["fridge_name", "chat_id"]

    def lookup_allowed(self, lookup: str, value: str) -> bool:
        if lookup == "fridge__relay__organization__id__exact":
            return True

        return super().lookup_allowed(lookup, value)

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["fridge__relay__organization"]

        return ()

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(fridge__relay__organization=request.user.organization)

    def formfield_for_foreignkey(
        self, db_field: ForeignKey[Any], request: HttpRequest | None, **kwargs: Any
    ) -> ModelChoiceField | None:
        if not request.user.is_admin:
            if db_field.name == "fridge":
                kwargs["queryset"] = Fridge.objects.filter(relay__organization=request.user.organization)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
