from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms.models import ModelChoiceField
from django.http import HttpRequest

from cryo_admin.accounts.models import User
from cryo_admin.cryo_gram import models


@admin.register(models.FridgeChatUser)
class FridgeChatUserAdmin(admin.ModelAdmin):
    """FridgeChatUser Admin"""

    @admin.display(description="Organization")
    def user_organization(self, obj):
        return obj.user.organization

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["user", "username", "user_organization"]

        return ["user", "username"]

    def lookup_allowed(self, lookup: str, value: str) -> bool:
        if lookup == "user__organization__id__exact":
            return True

        return super().lookup_allowed(lookup, value)

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["user__organization"]

        return []

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(user__organization=request.user.organization)

    def formfield_for_foreignkey(
        self, db_field: ForeignKey[Any], request: HttpRequest | None, **kwargs: Any
    ) -> ModelChoiceField | None:
        if not request.user.is_admin:
            if db_field.name == "user":
                kwargs["queryset"] = User.objects.filter(organization=request.user.organization)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
