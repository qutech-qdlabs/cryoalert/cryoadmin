from django.apps import AppConfig


class CryoGramConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "cryo_admin.cryo_gram"
