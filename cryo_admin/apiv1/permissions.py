from rest_framework_api_key.permissions import BaseHasAPIKey

from cryo_admin.apiv1.models import APIKey, CryoGramAPIKey


class HasAPIKey(BaseHasAPIKey):
    """Custom HasAPIKey permission class for custom APIKey model"""

    model = APIKey


class HasCryoGramAPIKey(BaseHasAPIKey):
    """Custom HasCryoGramAPIKey permission class for custom CryoGramAPIKey model"""

    model = CryoGramAPIKey
