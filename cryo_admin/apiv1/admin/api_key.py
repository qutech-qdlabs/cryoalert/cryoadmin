from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db import models
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms.models import ModelChoiceField
from django.http.request import HttpRequest
from django.utils import timezone
from rest_framework_api_key.admin import APIKeyModelAdmin as _APIKeyModelAdmin
from rest_framework_api_key.models import APIKey as _APIKey

from cryo_admin.apiv1.models import APIKey
from cryo_admin.fridges.models import Relay


class HasExpiredFilter(admin.SimpleListFilter):
    title = "Has Expired"
    parameter_name = "expiry_date"

    def lookups(self, request: Any, model_admin: Any) -> list[tuple[Any, str]]:
        return [
            ("true", "True"),
            ("false", "False"),
        ]

    def queryset(self, request: Any, queryset: QuerySet[Any]) -> QuerySet[Any] | None:
        if self.value() == "true":
            return queryset.filter(expiry_date__lt=timezone.now())

        if self.value() == "false":
            # Count No expiry_date as always not expired
            return queryset.filter(models.Q(expiry_date__gte=timezone.now()) | models.Q(expiry_date=None))


@admin.register(APIKey)
class APIKeyAdmin(_APIKeyModelAdmin):
    @admin.display(description="Organization")
    def organization(self, obj):
        return obj.relay.organization

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ("name", "organization", "created", "expiry_date", "_has_expired", "revoked")
        else:
            return ("name", "created", "expiry_date", "_has_expired", "revoked")

    def lookup_allowed(self, lookup: str, value: str) -> bool:
        if lookup == "relay__organization__id__exact":
            return True

        return super().lookup_allowed(lookup, value)

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["relay__organization", HasExpiredFilter]

        return (HasExpiredFilter,)

    def get_fieldsets(self, request: HttpRequest, obj: Any | None = ...) -> list[tuple[str | None, dict[str, Any]]]:
        if obj is None:
            return ((None, {"fields": ("name", "expiry_date", "relay")}),)
        return super().get_fieldsets(request, obj)

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(relay__organization=request.user.organization)

    def formfield_for_foreignkey(
        self, db_field: ForeignKey[Any], request: HttpRequest | None, **kwargs: Any
    ) -> ModelChoiceField | None:
        if not request.user.is_admin:
            if db_field.name == "relay":
                kwargs["queryset"] = Relay.objects.filter(organization=request.user.organization)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


if admin.site.is_registered(_APIKey):
    admin.site.unregister(_APIKey)
