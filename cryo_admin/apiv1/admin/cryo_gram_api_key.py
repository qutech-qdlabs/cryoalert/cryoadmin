from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db import models
from django.db.models.query import QuerySet
from django.http.request import HttpRequest
from django.utils import timezone
from rest_framework_api_key.admin import APIKeyModelAdmin as _APIKeyModelAdmin
from rest_framework_api_key.models import APIKey as _APIKey

from cryo_admin.apiv1.models import CryoGramAPIKey


class HasExpiredFilter(admin.SimpleListFilter):
    title = "Has Expired"
    parameter_name = "expiry_date"

    def lookups(self, request: Any, model_admin: Any) -> list[tuple[Any, str]]:
        return [
            ("true", "True"),
            ("false", "False"),
        ]

    def queryset(self, request: Any, queryset: QuerySet[Any]) -> QuerySet[Any] | None:
        if self.value() == "true":
            return queryset.filter(expiry_date__lt=timezone.now())

        if self.value() == "false":
            # Count No expiry_date as always not expired
            return queryset.filter(models.Q(expiry_date__gte=timezone.now()) | models.Q(expiry_date=None))


@admin.register(CryoGramAPIKey)
class CryoGramAPIKeyAdmin(_APIKeyModelAdmin):
    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        return ("name", "created", "expiry_date", "_has_expired", "revoked")

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        return (HasExpiredFilter,)

    def get_fieldsets(self, request: HttpRequest, obj: Any | None = ...) -> list[tuple[str | None, dict[str, Any]]]:
        if obj is None:
            return ((None, {"fields": ("name", "expiry_date")}),)
        return super().get_fieldsets(request, obj)


if admin.site.is_registered(_APIKey):
    admin.site.unregister(_APIKey)
