from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms.models import ModelChoiceField
from django.http.request import HttpRequest

from cryo_admin.apiv1.models import AlertUpdateResponse
from cryo_admin.fridges.models import Fridge


class FridgeFilter(admin.SimpleListFilter):
    title = "Fridge"
    parameter_name = "fridge_name"

    def lookups(self, request: Any, model_admin: Any) -> list[tuple[Any, str]]:
        if request.user.is_admin:
            fridge_names = Fridge.objects.values_list("name")
        else:
            fridge_names = Fridge.objects.filter(organization=request.user.organization).values_list("name")
        return [(fridge_name[0], fridge_name[0]) for fridge_name in fridge_names]

    def queryset(self, request: Any, queryset: QuerySet[Any]) -> QuerySet[Any] | None:
        if fridge_name := self.value():
            return queryset.filter(fridge__name=fridge_name)


@admin.register(AlertUpdateResponse)
class AlertUpdateResponseAdmin(admin.ModelAdmin):
    """AlertUpdateResponse Admin"""

    list_display = ["uuid", "fridge_name", "state", "date_modified"]

    @admin.display(empty_value="???")
    def fridge_name(self, obj):
        return obj.fridge.name

    @admin.display(description="Organization")
    def fridge_organization(self, obj):
        return obj.fridge.organization

    def lookup_allowed(self, lookup: str, value: str) -> bool:
        if lookup == "fridge__organization__id__exact":
            return True

        return super().lookup_allowed(lookup, value)

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["fridge__organization", FridgeFilter, "state"]

        return [FridgeFilter, "state"]

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["uuid", "fridge_name", "fridge_organization", "state", "date_modified"]

        return ["uuid", "fridge_name", "state", "date_modified"]

    def get_queryset(self, request: HttpRequest) -> QuerySet:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(fridge__organization=request.user.organization)

    def formfield_for_foreignkey(
        self, db_field: ForeignKey[Any], request: HttpRequest | None, **kwargs: Any
    ) -> ModelChoiceField | None:
        if not request.user.is_admin:
            if db_field.name == "fridge":
                kwargs["queryset"] = Fridge.objects.filter(relay__organization=request.user.organization)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
