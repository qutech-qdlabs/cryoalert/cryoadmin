from datetime import UTC, datetime

from django.utils import timezone
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView, status
from rest_framework_api_key.permissions import KeyParser

from cryo_admin.alerts.serializers import AlertCollectionSerializer
from cryo_admin.apiv1.models import AlertUpdateResponse, APIKey
from cryo_admin.apiv1.permissions import HasAPIKey


class AlertDefinitionView(APIView):
    permission_classes = [HasAPIKey]
    key_parser = KeyParser()

    def _clean(self) -> None:
        # Set all updateresponses that last longer than 5 minutes to "UNANSWERED"
        #
        (
            AlertUpdateResponse.objects.filter(state=AlertUpdateResponse.States.PENDING)
            .filter(date_modified__lte=(timezone.now() - timezone.timedelta(minutes=5)))
            .update(state=AlertUpdateResponse.States.UNANSWERED, date_modified=timezone.now())
        )

    def get(self, request: Request, format=None):
        key = self.key_parser.get(request)
        if not key:
            raise Exception("This should never occurr due to permission_classes settings")
        api_key: APIKey = APIKey.objects.get_from_key(key)

        # Clean up old pending responses
        self._clean()

        last_poll_time = request.GET.get("last_poll_time", None)
        if last_poll_time:
            last_poll_time = datetime.strptime(last_poll_time, "%Y-%m-%dT%H:%M:%S").replace(tzinfo=UTC)

        if not api_key.relay:
            return Response(
                status=status.HTTP_200_OK,
                content_type="application/json",
                data=[],
            )

        data = []
        for fridge in api_key.get_alert_update_active_fridges():
            latest_aur = AlertUpdateResponse.objects.get_latest(fridge)
            if (
                latest_aur
                and (last_poll_time or latest_aur.date_created) > fridge.get_latest_alert_rules_modification_date()
            ):
                continue

            new_aur = AlertUpdateResponse.objects.create(fridge=fridge)
            data.append(
                {
                    "url": fridge.url,
                    "update_uuid": new_aur.uuid,
                    "alerts": AlertCollectionSerializer(fridge.alert_def).data,
                }
            )

        return Response(
            status=status.HTTP_200_OK,
            content_type="application/json",
            data=data,
        )

    def post(self, request: Request, format=None):
        # Clean up old pending responses
        self._clean()
        for data in request.data:
            if uuid := data.get("uuid"):
                # Update in this verbose manner to trigger the save() method for updating the "date_modified" property
                aur: AlertUpdateResponse = AlertUpdateResponse.objects.get(uuid=uuid)
                if data.get("success", False):
                    aur.set_success()
                else:
                    aur.set_failure(error=data.get("error", ""))

        return Response(status=status.HTTP_200_OK)
