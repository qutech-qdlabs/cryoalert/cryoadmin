from .alert_definitions import AlertDefinitionView
from .chats import AllowedUserView, GetFridgeChatIDView, KnownFridgesView
from .service_discovery import PrometheusServiceDiscoveryView
