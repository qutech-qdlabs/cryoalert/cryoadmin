from urllib.parse import urlparse

from rest_framework.response import Response
from rest_framework.views import APIView, status
from rest_framework_api_key.permissions import KeyParser

from cryo_admin.apiv1.models import APIKey
from cryo_admin.apiv1.permissions import HasAPIKey


class PrometheusServiceDiscoveryView(APIView):
    permission_classes = [HasAPIKey]
    key_parser = KeyParser()

    def get(self, request, format=None):
        # This endpoint should always return a HTTP_200_OK, but with an empty list if something went wrong
        data = []

        # Get api key from request and extract all fridges from the corresponding relay
        if key := self.key_parser.get(request):
            api_key: APIKey = APIKey.objects.get_from_key(key)
            if api_key.relay:
                for fridge in api_key.get_sd_active_fridges():
                    data.append(
                        {
                            "targets": [urlparse(fridge.url).netloc],
                            "labels": {
                                "fridge": fridge.name.lower(),
                                "organization": api_key.relay.organization.name.lower(),
                                **fridge.extra_labels,
                            },
                        }
                    )

        # Always return a HTTP_200_OK (unless authentication fails, than thats a different problem)
        return Response(
            status=status.HTTP_200_OK,
            content_type="application/json",
            data=data,
        )
