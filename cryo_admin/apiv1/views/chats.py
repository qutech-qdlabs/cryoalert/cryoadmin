from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView, status

from cryo_admin.accounts.models import Organization
from cryo_admin.apiv1.permissions import HasCryoGramAPIKey
from cryo_admin.cryo_gram.models import FridgeChat, FridgeChatUser


class AllowedUserView(APIView):
    permission_classes = [HasCryoGramAPIKey]

    def get(self, request: Request, format=None):
        sending_username = request.GET.get("sending_username", None)
        if sending_username is None:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                content_type="application/json",
                data={"error_message": "Missing GET parameter `sending_username`"},
            )

        # Check if sending_username is known as a username
        if not FridgeChatUser.objects.filter(username=sending_username).exists():
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                content_type="application/json",
                data={"error_message": f"Username {sending_username} could not be found"},
            )

        return Response(
            status=status.HTTP_200_OK,
            content_type="application/json",
            data={},
        )


class KnownFridgesView(APIView):
    permission_classes = [HasCryoGramAPIKey]

    def get(self, request: Request, format=None):
        sending_username = request.GET.get("sending_username", None)
        if sending_username is None:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                content_type="application/json",
                data={"error_message": "Missing GET parameter `sending_username`"},
            )

        # Check if sending_username is known as a username
        if not FridgeChatUser.objects.filter(username=sending_username).exists():
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                content_type="application/json",
                data={"error_message": f"Username {sending_username} could not be found"},
            )

        # Obtain a list of fridge names the sending_username can access based on his account
        orgs_inner_qs = FridgeChatUser.objects.filter(username=sending_username).values("user__organization")
        orgs = Organization.objects.filter(id__in=orgs_inner_qs)
        fridge_names = list(
            FridgeChat.objects.filter(fridge__relay__organization__in=orgs)
            .filter(fridge__enable_cryogram_request=True)
            .values_list("fridge__name", flat=True)
        )

        if len(fridge_names) > 0:
            return Response(
                status=status.HTTP_200_OK,
                content_type="application/json",
                data=fridge_names,
            )
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)


class GetFridgeChatIDView(APIView):
    permission_classes = [HasCryoGramAPIKey]

    def _get_param(self, request: Request, param_name: str, required: bool = True) -> str | None:
        param = request.GET.get(param_name, None)
        if required and param is None:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                content_type="application/json",
                data={"error_message": f"Missing GET parameter `{param_name}`"},
            )

        return param

    def get(self, request: Request, format=None):
        sending_username = self._get_param(request, "sending_username", required=True)
        fridge_name = self._get_param(request, "fridge_name", required=True)
        org_name = self._get_param(request, "org_name", required=False)

        # If org name is provided, filter for that organization, but only if the a user account with the sending_username is an admin account
        fridge_user_accounts = FridgeChatUser.objects.filter(username=sending_username).all()
        is_admin = any([fridge_user.user.is_admin for fridge_user in fridge_user_accounts])

        if is_admin and not org_name:
            fridge_chat_qs = FridgeChat.objects.filter(fridge__enable_cryogram_request=True)
        if is_admin and org_name:
            if not Organization.objects.filter(name__iexact=org_name).exists():
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    content_type="application/json",
                    data={"error_message": f"No organization with name=`{org_name}` could be found"},
                )

            fridge_chat_qs = FridgeChat.objects.filter(fridge__relay__organization__name__iexact=org_name).filter(
                fridge__enable_cryogram_request=True
            )
        else:
            orgs_inner_qs = FridgeChatUser.objects.filter(username=sending_username).values("user__organization")
            orgs = Organization.objects.filter(id__in=orgs_inner_qs)
            fridge_chat_qs = FridgeChat.objects.filter(fridge__relay__organization__in=orgs).filter(
                fridge__enable_cryogram_request=True
            )

        if not fridge_chat_qs.filter(fridge__name__iexact=fridge_name).exists():
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                content_type="application/json",
                data={"error_message": f"No fridge with name=`{fridge_name}` could be found"},
            )

        return Response(
            status=status.HTTP_200_OK,
            content_type="application/json",
            data={"chat_id": fridge_chat_qs.get(fridge__name__iexact=fridge_name).chat_id},
        )
