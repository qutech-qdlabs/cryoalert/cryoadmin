from django.urls import include, path

from .views import (
    AlertDefinitionView,
    AllowedUserView,
    GetFridgeChatIDView,
    KnownFridgesView,
    PrometheusServiceDiscoveryView,
)

urlpatterns = [
    path("sd/", PrometheusServiceDiscoveryView.as_view()),
    path("alerts/", AlertDefinitionView.as_view()),
    path("allowed_user/", AllowedUserView.as_view()),
    path("known_fridges/", KnownFridgesView.as_view()),
    path("chat_id/", GetFridgeChatIDView.as_view()),
]
