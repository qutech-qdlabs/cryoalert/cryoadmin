from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication
from rest_framework_api_key.permissions import KeyParser

from cryo_admin.apiv1.models import APIKey, CryoGramAPIKey


class APIKeyAuthentication(BaseAuthentication):
    key_parser = KeyParser()

    def authenticate(self, request):
        key = self.key_parser.get(request)
        if key is None:
            return None

        try:
            api_key = APIKey.objects.get_from_key(key)
        except ObjectDoesNotExist:
            return None

        return (AnonymousUser(), api_key)


class CryoGramAPIKeyAuthentication(BaseAuthentication):
    key_parser = KeyParser()

    def authenticate(self, request):
        key = self.key_parser.get(request)
        if key is None:
            return None

        try:
            api_key = CryoGramAPIKey.objects.get_from_key(key)
        except ObjectDoesNotExist:
            return None

        return (AnonymousUser(), api_key)
