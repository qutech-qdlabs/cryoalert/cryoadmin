from django.core import management
from django.test import TestCase

from cryo_admin.accounts.models import Organization
from cryo_admin.apiv1.models import APIKey
from cryo_admin.fridges.models import Fridge, Relay


# Create your tests here.
class ServiceDiscoveryTestCase(TestCase):
    def setUp(self) -> None:
        # Initialize
        management.call_command("setup")

        # Create organizations
        empty_org = Organization.objects.create(name="empty_org")
        nonempty_org = Organization.objects.create(name="nonempty_org")

        # Create Relays
        empty_org_relay = Relay.objects.create(name="empty_org_relay", organization=empty_org)
        nonempty_org_relay = Relay.objects.create(name="nonempty_org_relay", organization=nonempty_org)

        # Create fridges
        for fridge_name in [f"fridge{idx}" for idx in range(10)]:
            Fridge.objects.create(
                name=fridge_name,
                organization=nonempty_org,
                relay=nonempty_org_relay,
                url="http://localhost:8081",
            )

        # Create API-keys
        _, self.empty_org_api_key = APIKey.objects.create_key(name="empty_org_relay_api_key", relay=empty_org_relay)
        _, self.nonempty_org_api_key = APIKey.objects.create_key(
            name="nonempty_org_relay_api_key", relay=nonempty_org_relay
        )

    def test_no_auth(self):
        response = self.client.get("/api/v1/sd/")
        self.assertEqual(response.status_code, 401)

    def test_no_fridges(self):
        response = self.client.get("/api/v1/sd/", headers={"Authorization": f"Api-Key {self.empty_org_api_key}"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_with_fridges(self):
        response = self.client.get("/api/v1/sd/", headers={"Authorization": f"Api-Key {self.nonempty_org_api_key}"})
        self.assertEqual(response.status_code, 200)

        sd_data = response.json()
        self.assertGreaterEqual(len(sd_data), 1)

        fridge_data = sd_data[0]
        self.assertTrue("targets" in fridge_data)
        self.assertTrue("labels" in fridge_data)
