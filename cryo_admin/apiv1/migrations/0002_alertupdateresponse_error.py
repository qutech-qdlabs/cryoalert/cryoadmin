# Generated by Django 4.2.11 on 2024-04-22 07:50

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("apiv1", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="alertupdateresponse",
            name="error",
            field=models.TextField(default=""),
        ),
    ]
