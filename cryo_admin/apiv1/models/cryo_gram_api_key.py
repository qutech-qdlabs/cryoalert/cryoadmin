from django.db import models
from rest_framework_api_key.models import AbstractAPIKey, BaseAPIKeyManager


class CryoGramAPIKeyManager(BaseAPIKeyManager):
    """Manager for the CryoGramAPIKey model"""


class CryoGramAPIKey(AbstractAPIKey):
    """Model representing an APIKey belonging to a CryoGram instance"""

    # Data

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)

    # Manager
    objects = CryoGramAPIKeyManager()

    class Meta(AbstractAPIKey.Meta):
        verbose_name = "CryoGram API key"
        verbose_name_plural = "CryoGram API keys"

    def __str__(self) -> str:
        return f"CryoGramAPIKey(name={self.name})"
