from django.db import models
from rest_framework_api_key.models import AbstractAPIKey, BaseAPIKeyManager


class APIKeyManager(BaseAPIKeyManager):
    """Manager for the APIKey model"""


class APIKey(AbstractAPIKey):
    """Model representing an APIKey belonging to a relay"""

    # Data
    relay = models.OneToOneField("fridges.Relay", related_name="api_key", on_delete=models.CASCADE)

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)

    # Manager
    objects = APIKeyManager()

    class Meta(AbstractAPIKey.Meta): ...

    def get_sd_active_fridges(self):
        return self.relay.fridges.filter(enable_service_discovery=True).all()

    def get_alert_update_active_fridges(self):
        return self.relay.fridges.filter(enable_alert_updates=True).all()

    def get_cryogram_active_fridges(self):
        return self.relay.fridges.filter(enable_cryogram_request=True).all()

    def __str__(self) -> str:
        return f"API-key for {self.relay}"
