from uuid import uuid4

from django.db import models


class AlertUpdateResponseManager(models.Manager):
    """Manager for the AlertUpdateResponse model"""

    def get_latest(self, fridge) -> "AlertUpdateResponse":
        """
        Get the latest AlertUpdateResponse for a given fridge.
        This includes only those updates that have not failed or are unanswered.
        """

        if (
            latest_aur := self.filter(fridge=fridge)
            .exclude(state__in=[AlertUpdateResponse.States.FAILURE, AlertUpdateResponse.States.UNANSWERED])
            .order_by("-date_created")
            .first()
        ):
            return latest_aur
        else:
            return None


class AlertUpdateResponse(models.Model):
    """Model representing an AlertUpdateResponse"""

    class States(models.IntegerChoices):
        PENDING = 0
        SUCCESS = 1
        FAILURE = 2
        UNANSWERED = 3

    # Data
    fridge = models.ForeignKey("fridges.Fridge", on_delete=models.CASCADE)
    uuid = models.UUIDField(default=uuid4, unique=True)
    state = models.IntegerField(choices=States.choices, default=States.PENDING)
    error = models.TextField(default="")

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    # Manager
    objects: AlertUpdateResponseManager = AlertUpdateResponseManager()

    @property
    def is_pending(self) -> bool:
        return self.state == self.States.PENDING

    @property
    def is_success(self) -> bool:
        return self.state == self.States.SUCCESS

    @property
    def is_failure(self) -> bool:
        return self.state == self.States.FAILURE

    @property
    def is_unanswered(self) -> bool:
        return self.state == self.States.UNANSWERED

    def set_pending(self) -> None:
        self.state = self.States.PENDING
        self.save()

    def set_success(self) -> None:
        self.state = self.States.SUCCESS
        self.save()

    def set_failure(self, error: str = None) -> None:
        self.state = self.States.FAILURE
        if error:
            self.error = error
        self.save()

    def set_unanswered(self) -> None:
        self.state = self.States.UNANSWERED
        self.save()

    def set_error(self, error: str) -> None:
        self.error = error
        self.save()

    def __str__(self) -> str:
        return f"AlertUpdateResponse(uuid={self.uuid}, state={self.state})"
