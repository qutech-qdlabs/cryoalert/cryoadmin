from django.core import management
from django.test import TestCase

from cryo_admin.accounts.models import Organization, Role, User


# Create your tests here.
class RoleTestCase(TestCase):
    def setUp(self):
        management.call_command("setup")

    def test_exist_viewer_role(self):
        Role.objects.get(name="viewer")

    def test_exist_editor_role(self):
        Role.objects.get(name="editor")

    def test_exist_org_admin_role(self):
        Role.objects.get(name="org_admin")

    def test_exist_admin_role(self):
        Role.objects.get(name="admin")
