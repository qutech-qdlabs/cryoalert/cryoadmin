from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as _UserAdmin
from django.contrib.auth.models import Group
from django.db.models import Q
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms.models import ModelChoiceField
from django.http.request import HttpRequest
from django.utils.translation import gettext_lazy as _

from cryo_admin.accounts.models import Organization, Role, User

# Unregister group
admin.site.unregister(Group)


# Register your models here.
@admin.register(User)
class UserAdmin(_UserAdmin):
    inlines = []
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "username",
                    "password1",
                    "password2",
                    "role",
                    "organization",
                ),
            },
        ),
    )

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["organization", "role", "is_active"]

        return ["role", "is_active"]

    @admin.display(boolean=True, description="is adminstator?")
    def is_administator(self, obj):
        return obj.is_admin

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        default_list = ["username", "role", "is_administator"]

        if request.user.is_admin:
            default_list.insert(1, "organization")
            return default_list

        return default_list

    def get_fieldsets(self, request: HttpRequest, obj: Any | None = None) -> list[tuple[str | None, dict[str, Any]]]:
        if obj is None:
            return super().get_fieldsets(request, obj)

        none_fields = (None, {"fields": ("username", "password")})
        org_fields = (_("Organization"), {"fields": ("organization",)})
        personal_fields = (_("Personal info"), {"fields": ("first_name", "last_name", "email")})
        permissions_fields = (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "role",
                ),
            },
        )
        dates_fields = (_("Important dates"), {"fields": ("last_login", "date_joined")})

        if request.user.is_admin:
            return (
                none_fields,
                org_fields,
                personal_fields,
                permissions_fields,
                dates_fields,
            )

        return (
            none_fields,
            personal_fields,
            permissions_fields,
            dates_fields,
        )

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(organization=request.user.organization)

    def formfield_for_foreignkey(
        self, db_field: ForeignKey[Any], request: HttpRequest | None, **kwargs: Any
    ) -> ModelChoiceField | None:
        if not request.user.is_admin:
            if db_field.name == "organization":
                kwargs["queryset"] = Organization.objects.filter(name=request.user.organization.name)
                kwargs["initial"] = request.user.organization

            if db_field.name == "role":
                if request.user.is_org_admin:
                    kwargs["queryset"] = Role.objects.filter(
                        Q(name__iexact="viewer") | Q(name__iexact="editor") | Q(name__iexact="org_admin")
                    )
                elif request.user.is_editor:
                    kwargs["queryset"] = Role.objects.filter(Q(name__iexact="viewer") | Q(name__iexact="editor"))
                elif request.user.is_viewer:
                    kwargs["queryset"] = Role.objects.filter(Q(name__iexact="viewer"))

                kwargs["initial"] = Role.objects.get(name__iexact="viewer")

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
