from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db.models.query import QuerySet
from django.http.request import HttpRequest

from cryo_admin.accounts.models import Organization


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    """Organization Admin"""

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["name", "num_users", "default", "date_created"]

        return ["name", "num_users", "date_created"]

    def get_fieldsets(self, request: HttpRequest, obj: Any | None = ...) -> list[tuple[str | None, dict[str, Any]]]:
        if request.user.is_admin:
            return super().get_fieldsets(request, obj)

        return ((None, {"fields": ("name",)}),)

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(id=request.user.organization.id)
