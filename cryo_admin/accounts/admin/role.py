from django.contrib import admin

from cryo_admin.accounts.models import Role


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    """Role Admin"""

    list_display = ["name"]
    filter_horizontal = ("permissions",)
