import operator
from functools import reduce
from typing import Any, TypedDict

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand, CommandParser
from django.db import models

from cryo_admin.accounts.models import Organization, Role


class AppPermission(TypedDict):
    app_label: str
    permissions: list[str] | dict[str, list[str]]


class RoleData(TypedDict):
    role_name: str
    app_permissions: list[AppPermission]


ROLES: list[RoleData] = [
    {
        "role_name": "viewer",
        "app_permissions": [
            {
                "app_label": "alerts",
                "permissions": ["view"],
            },
            {
                "app_label": "cryo_gram",
                "permissions": ["view"],
            },
            {
                "app_label": "fridges",
                "permissions": ["view"],
            },
        ],
    },
    {
        "role_name": "editor",
        "app_permissions": [
            {
                "app_label": "alerts",
                "permissions": ["view", "change"],
            },
            {
                "app_label": "cryo_gram",
                "permissions": ["view", "change"],
            },
            {
                "app_label": "fridges",
                "permissions": ["view", "change"],
            },
        ],
    },
    {
        "role_name": "org_admin",
        "app_permissions": [
            {
                "app_label": "accounts",
                "permissions": {
                    "User": ["view", "change", "add", "delete"],
                    "Organization": ["view", "change"],
                },
            },
            {
                "app_label": "alerts",
                "permissions": ["view", "change", "add", "delete"],
            },
            {
                "app_label": "apiv1",
                "permissions": {
                    "AlertUpdateResponse": ["view", "change", "add", "delete"],
                    "APIKey": ["view", "change", "add", "delete"],
                    "CryoGramAPIKey": ["view"],
                },
            },
            {
                "app_label": "cryo_gram",
                "permissions": ["view", "change", "add", "delete"],
            },
            {
                "app_label": "fridges",
                "permissions": ["view", "change", "add", "delete"],
            },
        ],
    },
    {
        "role_name": "admin",
        "app_permissions": [
            {
                "app_label": "accounts",
                "permissions": ["view", "change", "add", "delete"],
            },
            {
                "app_label": "alerts",
                "permissions": ["view", "change", "add", "delete"],
            },
            {
                "app_label": "apiv1",
                "permissions": ["view", "change", "add", "delete"],
            },
            {
                "app_label": "cryo_gram",
                "permissions": ["view", "change", "add", "delete"],
            },
            {
                "app_label": "fridges",
                "permissions": ["view", "change", "add", "delete"],
            },
        ],
    },
]


class Command(BaseCommand):
    help = "Setups the needed groups and roles for users"

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument("--default_org_name", type=str, default="default")

    def handle(self, *args: Any, **options: Any) -> str | None:
        # First, create default organization
        self._create_default_org(default_org_name=options["default_org_name"])

        # Then, create default roles
        self._create_roles()

    def _create_default_org(self, default_org_name) -> None:
        Organization.objects.get_or_create(name=default_org_name, default=True)

    def _create_roles(self) -> None:
        for role_data in ROLES:
            role, _ = Role.objects.get_or_create(name=role_data["role_name"])
            role.permissions.add(*self.get_permission_for_role(role_data))

    def get_permission_for_role(self, role: RoleData):
        perms = set()
        for app_permission in role["app_permissions"]:
            app_label = app_permission["app_label"]
            permissions = app_permission["permissions"]
            if isinstance(permissions, list):
                app_model_contenttypes = ContentType.objects.filter(app_label=app_label)
                query = reduce(operator.or_, (models.Q(codename__icontains=permission) for permission in permissions))
                perms.update(Permission.objects.filter(models.Q(content_type__in=app_model_contenttypes) & query).all())
            elif isinstance(permissions, dict):
                for model_name, model_permissions in permissions.items():
                    app_model_contenttypes = ContentType.objects.filter(app_label=app_label, model=model_name.lower())
                    query = reduce(
                        operator.or_, (models.Q(codename__icontains=permission) for permission in model_permissions)
                    )
                    perms.update(
                        Permission.objects.filter(models.Q(content_type__in=app_model_contenttypes) & query).all()
                    )

        return perms
