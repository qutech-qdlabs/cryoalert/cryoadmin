from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import Permission


class RoleModelBackend(ModelBackend):
    def _get_role_permissions(self, user_obj):
        if user_obj.role is None:
            return Permission.objects.none()

        return user_obj.role.permissions.all()

    def get_role_permissions(self, user_obj, obj=None):
        return self._get_permissions(user_obj, obj, "role")

    def get_all_permissions(self, user_obj, obj=None):
        if not user_obj.is_active or user_obj.is_anonymous or obj is not None:
            return set()
        if not hasattr(user_obj, "_perm_cache"):
            user_obj._perm_cache = self._get_all_permissions(user_obj)
        return user_obj._perm_cache

    def _get_all_permissions(self, user_obj, obj=None):
        return {
            *self.get_user_permissions(user_obj, obj=obj),
            *self.get_group_permissions(user_obj, obj=obj),
            *self.get_role_permissions(user_obj, obj=obj),
        }
