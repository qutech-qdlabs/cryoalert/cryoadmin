from django.contrib.auth.models import Permission
from django.db import models


class RoleManager(models.Manager):
    """Manager for the Role model"""


class Role(models.Model):
    """Model of a Role"""

    # Data
    name = models.CharField(max_length=100, unique=True)
    permissions = models.ManyToManyField(Permission, verbose_name="permissions", blank=True)

    # Manager
    objects = RoleManager()

    class Meta:
        verbose_name = "role"
        verbose_name_plural = "roles"

    @property
    def is_admin_role(self) -> bool:
        return self.name.lower() == "admin"

    @property
    def is_org_admin_role(self) -> bool:
        return self.name.lower() == "org_admin"

    @property
    def is_editor_role(self) -> bool:
        return self.name.lower() == "editor"

    @property
    def is_viewer_role(self) -> bool:
        return self.name.lower() == "viewer"

    def __str__(self) -> str:
        if self.is_admin_role:
            return "Admin"

        if self.is_org_admin_role:
            return "Organization Admin"

        if self.is_editor_role:
            return "Editor"

        if self.is_viewer_role:
            return "Viewer"

        return self.name
