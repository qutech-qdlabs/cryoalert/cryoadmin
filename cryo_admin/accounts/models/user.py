from django.apps import apps
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager as _UserManager
from django.db import models
from django.utils.translation import gettext_lazy as _


def get_default_org():
    return apps.get_model("accounts", "Organization").objects.get_default()


def get_viewer_role():
    return apps.get_model("accounts", "Role").objects.get(name="viewer")


class UserManager(_UserManager):
    """
    Custom Manager for the User model
    """


# Create your models here.
class User(AbstractUser):
    """Custom User Model"""

    # Data
    organization = models.ForeignKey("accounts.Organization", related_name="users", on_delete=models.CASCADE)
    role = models.ForeignKey("accounts.Role", on_delete=models.CASCADE)

    is_staff = models.BooleanField(
        _("staff status"),
        default=True,
        help_text=_("Designates whether the user can log into this admin site."),
    )

    # Manager
    objects = UserManager()

    REQUIRED_FIELDS = []

    @property
    def is_admin(self) -> bool:
        return self.is_superuser or self.role.is_admin_role

    @property
    def is_org_admin(self) -> bool:
        return self.role.is_org_admin_role

    @property
    def is_editor(self) -> bool:
        return self.role.is_editor_role

    @property
    def is_viewer(self) -> bool:
        return self.role.is_viewer_role
