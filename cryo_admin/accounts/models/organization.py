from django.db import models


class OrganizationManager(models.Manager):
    """Manager for the Organization model"""

    def get_default(self):
        return self.filter(default=True).first()


class Organization(models.Model):
    """Model of Organizations"""

    # Data
    name = models.CharField(max_length=200, unique=True)
    default = models.BooleanField(default=False, blank=True)

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)

    # Manager
    objects = OrganizationManager()

    class Meta:
        verbose_name = "organization"
        verbose_name_plural = "organizations"
        ordering = ["-date_created"]

    @property
    def num_users(self) -> int:
        return self.users.count()

    def save(self, *args, **kwargs) -> None:
        # Override to make sure only a single organization is the default organization

        if self.default:
            # Could be because it was already default or was set to default now
            qs = Organization.objects.filter(default=True).exclude(name=self.name)
            if qs.count() >= 1:
                # If another organization is already the default, set it to be not the default
                qs.update(default=False)

        return super().save(*args, **kwargs)

    def __str__(self) -> str:
        return self.name
