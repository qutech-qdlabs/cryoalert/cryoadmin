from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms.models import ModelChoiceField
from django.http.request import HttpRequest

from cryo_admin.accounts.models import Organization
from cryo_admin.fridges.models import Fridge, Relay


class FridgeInline(admin.TabularInline):
    model = Fridge
    extra = 0

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_fieldsets(self, request: HttpRequest, obj: Any | None = ...) -> list[tuple[str | None, dict[str, Any]]]:
        if request.user.is_admin:
            return super().get_fieldsets(request, obj)

        return ((None, {"fields": ("name", "url", "alert_def")}),)


@admin.register(Relay)
class RelayAdmin(admin.ModelAdmin):
    list_display = ["name"]
    extra_admin_list_display = ["organization"]
    inlines = [FridgeInline]

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return self.list_display + self.extra_admin_list_display
        return self.list_display

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["organization"]
        return []

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(organization=request.user.organization)

    def formfield_for_foreignkey(
        self, db_field: ForeignKey[Any], request: HttpRequest | None, **kwargs: Any
    ) -> ModelChoiceField | None:
        if not request.user.is_admin:
            if db_field.name == "organization":
                kwargs["queryset"] = Organization.objects.filter(name=request.user.organization.name)
                kwargs["initial"] = request.user.organization

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
