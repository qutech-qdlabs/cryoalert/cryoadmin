from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms.models import ModelChoiceField
from django.http.request import HttpRequest

from cryo_admin.accounts.models import Organization
from cryo_admin.alerts.models import AlertCollection
from cryo_admin.fridges.models import Fridge, Relay


class RelayFilter(admin.SimpleListFilter):
    title = "Relay"
    parameter_name = "relay"

    def lookups(self, request: Any, model_admin: Any) -> list[tuple[Any, str]]:
        if request.user.is_admin:
            relay_names = Relay.objects.values_list("name")
        else:
            relay_names = Relay.objects.filter(organization=request.user.organization).values_list("name")

        return [(relay_name[0], relay_name[0]) for relay_name in relay_names]

    def queryset(self, request: Any, queryset: QuerySet[Any]) -> QuerySet[Any] | None:
        if relay_name := self.value():
            return queryset.filter(relay__name=relay_name)


@admin.register(Fridge)
class FridgeAdmin(admin.ModelAdmin):
    """Fridge Admin"""

    @admin.display(description="Relay Name")
    def relay_name(self, obj):
        return obj.relay.name

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["name", "url", "organization", "relay_name"]

        return ["name", "url", "relay_name"]

    def lookup_allowed(self, lookup: str, value: str) -> bool:
        if lookup == "relay__organization__id__exact":
            return True

        return super().lookup_allowed(lookup, value)

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return [RelayFilter, "relay__organization"]

        return [RelayFilter]

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(relay__organization=request.user.organization)

    def formfield_for_foreignkey(
        self, db_field: ForeignKey[Any], request: HttpRequest | None, **kwargs: Any
    ) -> ModelChoiceField | None:
        if not request.user.is_admin:
            if db_field.name == "relay":
                kwargs["queryset"] = Relay.objects.filter(organization=request.user.organization)
            elif db_field.name == "alert_def":
                kwargs["queryset"] = AlertCollection.objects.filter(organization=request.user.organization)
            elif db_field.name == "organization":
                kwargs["queryset"] = Organization.objects.filter(name=request.user.organization.name)
                kwargs["initial"] = request.user.organization

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
