from datetime import datetime

from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.utils import timezone


class FridgeManager(models.Manager):
    """Custom Manager for the Fridge model"""


class Fridge(models.Model):
    """Model representing a fridge"""

    # Data
    name = models.CharField(max_length=100)
    organization = models.ForeignKey(
        "accounts.Organization",
        related_name="fridges",
        null=False,
        on_delete=models.CASCADE,
        verbose_name="organization",
    )
    url = models.URLField()
    relay = models.ForeignKey("fridges.Relay", related_name="fridges", on_delete=models.SET_NULL, null=True, blank=True)
    alert_def = models.ForeignKey(
        "alerts.AlertCollection", related_name="fridges", on_delete=models.SET_NULL, null=True, blank=True
    )
    extra_labels = models.JSONField(encoder=DjangoJSONEncoder, blank=True, default=dict)

    # Flags
    enable_service_discovery = models.BooleanField(default=True)
    enable_alert_updates = models.BooleanField(default=True)
    enable_cryogram_request = models.BooleanField(default=True)

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_alert_modified = models.DateTimeField(default=timezone.now)

    # Manager
    objects = FridgeManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["name", "organization"], name="unique_fridge_name_per_organization"),
        ]

    def save(self, *args, **kwargs):
        try:
            if not self.pk:
                #  make sure this is the exact date when the fridge record is created
                self.date_alert_modified = timezone.now()
            else:
                # check if `alert_def` or `relay` fields have changed for existing instances
                original = Fridge.objects.get(pk=self.pk)
                if self.alert_def != original.alert_def or self.relay != original.relay:
                    self.date_alert_modified = timezone.now()
        except Exception:
            self.date_alert_modified = timezone.now()

        super().save(*args, **kwargs)

    def get_latest_alert_rules_modification_date(self) -> datetime:
        latest_modification = self.date_alert_modified
        if self.alert_def:
            latest_modification = max(self.date_alert_modified, self.alert_def.get_latest_date_modified())

        return latest_modification

    def __str__(self) -> str:
        return f"Fridge(name={self.name})"
