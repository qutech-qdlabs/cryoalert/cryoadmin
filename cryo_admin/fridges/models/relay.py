from django.db import models


class RelayManager(models.Manager):
    """Manager for the Relay Model"""


class Relay(models.Model):
    """Model representing a relay"""

    # Data
    name = models.CharField(max_length=200)
    organization = models.ForeignKey(
        "accounts.Organization",
        related_name="relays",
        null=False,
        on_delete=models.CASCADE,
        verbose_name="organization",
    )

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)

    # Manager
    objects = RelayManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["name", "organization"], name="unique_relay_name_per_org"),
        ]

    def __str__(self) -> str:
        return f"Relay(name={self.name}, org={self.organization})"
