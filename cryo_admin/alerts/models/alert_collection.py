from datetime import datetime

from django.db import models


class AlertCollectionManager(models.Manager):
    """Manager for the AlertCollection Model"""


class AlertCollection(models.Model):
    """Model representing an Alert Collection"""

    # Data
    name = models.CharField(max_length=200)
    rules = models.ManyToManyField("alerts.AlertRule", related_name="collection")
    organization = models.ForeignKey(
        "accounts.Organization", related_name="alert_collections", on_delete=models.CASCADE, verbose_name="Organization"
    )

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    # Manager
    objects: AlertCollectionManager = AlertCollectionManager()

    class Meta: ...

    def get_latest_date_modified(self) -> datetime:
        """Get the latest modified date from itself and all its rules"""
        latest_rule_updated = self.rules.order_by("-date_modified").first()
        if latest_rule_updated and latest_rule_updated.date_modified > self.date_modified:
            return latest_rule_updated.date_modified

        return self.date_modified

    def __str__(self) -> str:
        return f"AlertCollection(name={self.name})"
