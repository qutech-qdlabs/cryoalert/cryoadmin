from django.db import models


class AlertRuleManager(models.Manager):
    """Manager for the AlertRule Model"""


class AlertRule(models.Model):
    """Model representing an Alert Rule"""

    # Data
    organization = models.ForeignKey("accounts.Organization", related_name="alert_rules", on_delete=models.CASCADE)
    alert_name = models.CharField(max_length=200)
    unit = models.CharField(max_length=100, null=True, blank=True, default=None)
    default_enabled = models.BooleanField(default=True)

    bounds_upper_enabled = models.BooleanField(default=False)
    bounds_upper_default_enabled = models.BooleanField(default=False)
    bounds_upper_default_value = models.FloatField(null=True, blank=True, default=None)

    bounds_lower_enabled = models.BooleanField(default=False)
    bounds_lower_default_enabled = models.BooleanField(default=False)
    bounds_lower_default_value = models.FloatField(null=True, blank=True, default=None)

    # Logging
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    # Manager
    objects = AlertRuleManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["alert_name", "organization"], name="unique_alert_name_per_organization"),
        ]

    @property
    def bounds(self):
        ret = {}
        if self.bounds_upper_enabled:
            ret.update(
                {
                    "upper": {"default_enabled": self.bounds_upper_default_enabled},
                    "default_value": self.bounds_upper_default_value,
                }
            )

        if self.bounds_lower_enabled:
            ret.update(
                {
                    "lower": {"default_enabled": self.bounds_lower_default_enabled},
                    "default_value": self.bounds_lower_default_value,
                }
            )

        return ret

    def __str__(self) -> str:
        return f"AlertRule(name={self.alert_name}, org={self.organization})"
