from rest_framework import serializers

from cryo_admin.alerts.models import AlertRule


class AlertRuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlertRule
        exclude = ["id", "organization", "date_created", "date_modified"]
