from rest_framework import serializers

from cryo_admin.alerts.models import AlertCollection, AlertRule


class AlertCollectionSerializer(serializers.ModelSerializer):
    rules = serializers.SerializerMethodField()

    class Meta:
        model = AlertCollection
        fields = ["name", "rules"]

    def get_rules(self, obj: AlertCollection):
        data = []
        for rule in obj.rules.all():
            rule: AlertRule = rule
            rule_data = {
                "alert_name": rule.alert_name,
                "unit": rule.unit,
                "default_enabled": rule.default_enabled,
                "bounds": None,
            }

            if rule.bounds_upper_enabled or rule.bounds_lower_enabled:
                bounds = {}
                if rule.bounds_upper_enabled:
                    bounds.update(
                        {
                            "upper": {
                                "default_enabled": rule.bounds_upper_default_enabled,
                                "default_value": rule.bounds_upper_default_value,
                            }
                        }
                    )

                if rule.bounds_lower_enabled:
                    bounds.update(
                        {
                            "lower": {
                                "default_enabled": rule.bounds_lower_default_enabled,
                                "default_value": rule.bounds_lower_default_value,
                            }
                        }
                    )

                rule_data["bounds"] = bounds
            data.append(rule_data)

        return data
