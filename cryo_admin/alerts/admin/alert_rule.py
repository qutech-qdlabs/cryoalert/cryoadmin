from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms.models import ModelChoiceField
from django.http.request import HttpRequest

from cryo_admin.accounts.models import Organization
from cryo_admin.alerts import models


@admin.register(models.AlertRule)
class AlertRuleAdmin(admin.ModelAdmin):
    """AlertRule Admin"""

    fieldsets = [
        (None, {"fields": ["organization", "alert_name", "unit", "default_enabled"]}),
        (
            "Upper Bounds",
            {"fields": ["bounds_upper_enabled", "bounds_upper_default_enabled", "bounds_upper_default_value"]},
        ),
        (
            "Lower Bounds",
            {"fields": ["bounds_lower_enabled", "bounds_lower_default_enabled", "bounds_lower_default_value"]},
        ),
    ]

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(organization=request.user.organization)

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ("alert_name", "organization")
        else:
            return ("alert_name",)

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["organization"]

        return []

    def formfield_for_foreignkey(
        self, db_field: ForeignKey[Any], request: HttpRequest | None, **kwargs: Any
    ) -> ModelChoiceField | None:
        if not request.user.is_admin:
            if db_field.name == "organization":
                kwargs["queryset"] = Organization.objects.filter(name=request.user.organization.name)
                kwargs["initial"] = request.user.organization

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
