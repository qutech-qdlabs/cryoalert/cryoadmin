from collections.abc import Sequence
from typing import Any

from django.contrib import admin
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms.models import ModelChoiceField, ModelMultipleChoiceField
from django.http.request import HttpRequest

from cryo_admin.accounts.models import Organization
from cryo_admin.alerts import models


@admin.register(models.AlertCollection)
class AlertCollectionAdmin(admin.ModelAdmin):
    """AlertCollection Admin"""

    list_display = ["name", "organization"]
    filter_horizontal = ("rules",)

    @admin.display(description="Number of Alert Rules")
    def num_rules(self, obj):
        return obj.rules.all().count()

    def get_list_display(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["name", "num_rules", "organization"]

        return ["name", "num_rules"]

    def get_list_filter(self, request: HttpRequest) -> Sequence[str]:
        if request.user.is_admin:
            return ["organization"]

        return ()

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request)
        if request.user.is_admin:
            return qs
        return qs.filter(organization=request.user.organization)

    def formfield_for_foreignkey(
        self, db_field: ForeignKey[Any], request: HttpRequest | None, **kwargs: Any
    ) -> ModelChoiceField | None:
        if not request.user.is_admin:
            if db_field.name == "organization":
                kwargs["queryset"] = Organization.objects.filter(name=request.user.organization.name)
                kwargs["initial"] = request.user.organization

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(
        self, db_field, request: HttpRequest | None, **kwargs: Any
    ) -> ModelMultipleChoiceField:
        if not request.user.is_admin:
            if db_field.name == "rules":
                kwargs["queryset"] = models.AlertRule.objects.filter(organization=request.user.organization)

        return super().formfield_for_manytomany(db_field, request, **kwargs)
