# Generated by Django 4.2.13 on 2024-08-20 14:20

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("alerts", "0005_alter_alertrule_unit"),
    ]

    operations = [
        migrations.AddConstraint(
            model_name="alertrule",
            constraint=models.UniqueConstraint(
                fields=("alert_name", "organization"), name="unique_alert_name_per_organization"
            ),
        ),
    ]
