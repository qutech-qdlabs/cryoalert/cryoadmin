#!/bin/bash 
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

# Local variables
run_mode=true

# Function to display script usage
usage()
{
    echo ""
    echo "Usage: $0 [OPTIONS]"
    echo "Options: "
    echo " -h   Display again this help message"
    echo " -s   Run the setup commands"
    exit 1
}

while getopts "hs" opt; do 
    case "$opt" in 
        h)
            usage
            ;;
        s) 
            run_mode=false 
            ;;
        *) 
            usage 
            ;;
    esac
done

if [ "$run_mode" = true ]; then 
    printf "...\n%s - running database migration" "$(date)"

    # run startup scripts
    python ./manage.py migrate 
    python ./manage.py collectstatic --no-input 

    printf "\n\n%s - starting server...\n\n" "$(date)"

    # Run app 
    gunicorn -b 0.0.0.0:8000 core.wsgi
else
    printf "...\n%s - running setup" "$(date)"

    # run setup scripts
    python ./manage.py migrate
    python ./manage.py setup
    python ./manage.py createadmin

    printf "...\n%s - completed setup" "$(date)"
fi

