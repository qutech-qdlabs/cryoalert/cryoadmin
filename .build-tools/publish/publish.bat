@echo off 
REM To be run in root folder as .build-support/publish/publish.bat

REM =======================
REM Obtain package version
REM =======================
REM run commands for getting the versions of the package
REM generate ESC character for some reason
for /F %%a in ('echo prompt $E ^| cmd') do @set "ESC=%%a"
for /f %%a in ('poetry run python -c "import cryo_admin; print(cryo_admin.__version__)"') do set "package_version=%%a"

REM =======================
REM Build and publish containers
REM =======================
echo Build and publish containers
docker compose build cryoadmin
docker tag registry.tudelft.nl/cryo_alert/cryo_admin:latest registry.tudelft.nl/cryo_alert/cryo_admin:%package_version%
docker push registry.tudelft.nl/cryo_alert/cryo_admin:latest
docker push registry.tudelft.nl/cryo_alert/cryo_admin:%package_version%