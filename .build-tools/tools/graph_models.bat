@echo off

REM graph models
cd cryo_admin
python .\manage.py graph_models -X ContentType,AbstractUser,Permission,AbstractAPIKey  --pygraphviz -g -o .\_graph_models\cryo_admin.png