@echo off

REM run pygount for SLOC
pygount --format=summary -F staticfiles,migrations,_graph_models,__pycache__,.git,tests --duplicates ./cryo_admin/