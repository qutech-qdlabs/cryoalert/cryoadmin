@echo off 

REM run trivy via container because no windows installation exists
docker run -v /var/run/docker.sock:/var/run/docker.sock -v %USERPROFILE%/Library/Caches:/root/.cache/ aquasec/trivy image %1