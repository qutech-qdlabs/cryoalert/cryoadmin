# Roles

Within CryoAdmin, several user roles have been defined; `Admin`, `Organization Admin`, `Editor`, and `Viewer`. Note that within Django, "viewing" data models is also an assigned permission. This permission is not mentioned here explicitly, unless it is the only permission for that particular data model. Furtheremore, due to varying permissions depending on the role of the user, what is displayed in the web UI will differ between different users.

## Admin

The `Admin` role is assigned to system administrators. Users with this role will have all permissions and can view and edit all organizations.

## Organization Admin

The `Organization Admin` role is assigned to users who manage an organization. This role has the following permissions:

- Change Organization settings
- Add, change, and delete Users
- Add, change, and delete Fridges
- Add, change, and delete Relays
- Add, change, and delete Alert Rules
- Add, change, and delete Alert Collections
- Add, change, and delete AlertUpdateResponses
- Add, change, and delete API keys
- Add, change, and delete CryoGram API keys
- Add, change, and delete Fridge Chats
- Add, change, and delete Fridge Chat Users

## Editor

The `Editor` role is assigned to users who will be maintaining the current configuration. This role has the following permissions:

- Change Fridges
- Change Relays
- Change Alert Rules
- Change Alert Collections
- Change Fridge Chats
- Change Fridge Chat Users

## Viewer

The `Viewer` role is assigned to all users who are not assigned any other role. As the name implies, it can only view the current configuration. This role has the following permissions:

- View Fridges
- View Relays
- View Alert Rules
- View Alert Collections
- View Fridge Chats
- View Fridge Chat Users
