# Add CryoGram API-Key

A CryoGram API-Key is meant as an authenication and authorization method for a [CryoGram](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryogram) bot. Usually there should be only one in the system, but more can be added if needed.

## Overview

![CryoGram API Key overview](../img/CryoGramAPIKeyOverview.png)

## Add new CryoGram API-Key

To create a new CryoGram API-key, fill in the form as shown below. It is not required to add an expiry date.

![Add CryoGram API Key](../img/AddCryoGramAPIKey.png)

After saving, the CryoGram API-key is shown as a message in the browser (see below). This is shown only once and cannot be retrieved afterwards. Be sure to save it!

![New Cryogram APi Key](../img/NewCryoGramApiKey.png)
