# Add Fridge Telegram Group Chat

## Creating Group chat and obtaining its chat ID

Before adding a Telegram group chat to CryoAdmin, create the group chat and add CryoAlertBot (or your Telegram bot) to the group as administrator. Then, send the `\getchatid` command to the bot to determine the `chatid` of the group chat. It should be a large negative integer.

## Overview

![Fridge Chat Overview](../img/FridgeChatOverview.png)

## Add new Fridge Chat

![Add new Fridge Chat](../img/AddFridgeChat.png)
