# Add new User

Adding a new user can be done by any user with the `Admin` or `Organization Admin` roles. On the home page, click on the "Users" text to go the the users overview, or click on the add button to directly go to the form for creating a new user.

![add_user1](../img/AddUser1.png)

On the user overview page, you can find a button to add a new user on the right side of the page.

![add_user_overview](../img/AddUserOverview.png)

In either case, you will be redirected to the form for creating a new user.

![add_user2](../img/AddUser2.png)
