# Home Page

The home page is the first page you will see after login. From here, you can select the data model you wish and have an overview of the recent actions you have performed. On the top right, you can logout and change your password, if you wish to do so.

![home_page](../img/HomePage.png)
