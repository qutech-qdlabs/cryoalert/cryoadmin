# Usage

- [Home Page](home_page.md)
- [Roles Description](roles.md)
- [Add new Organization](new_org.md)
- [Add new User](new_user.md)
- [Add new Relay](new_relay.md)
- [Add new API-Key](new_api_key.md)
- [Add new Fridge](new_fridge.md)
- [Add new Alert rules and Collections](new_alert_rule.md)
- [Add new CryoGram API-Key](new_cryogram_apikey.md)
- [Add new Telegram User](new_telegram_user.md)
- [Add new Telegram fridge group chat](new_fridge_group_chat.md)
