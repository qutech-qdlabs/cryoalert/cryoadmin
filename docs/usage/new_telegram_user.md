# Add Fridge Chat User

Before a user can be added as a fridge chat user, they must create their Telegram account with a searchable username.

## Overview

![Fridge Chat User Overview](../img/FridgeChatUserOverview.png)

## Add New Fridge Chat User

![Add Fridge Chat User](../img/AddFridgeChatUser.png)
