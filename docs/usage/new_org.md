# Add new Organization

Adding an organization can only be done by a user with the `Admin` role. On the home page, click on the "Organizations" text to go the the organization overview, or click on the add button to directly go to the form for creating a new organization.

![add_org1](../img/AddOrg1.png)

On the organization overview page, you can find a button to add a new organization on the right side of the page.

![add_org_overview](../img/AddOrgOverview.png)

In either case, you will be redirected to the form for creating a new organization. Note that, by selecting the new organization to be the default, any other organization that is currently the default won't be the default anymore.

![add_org2](../img/AddOrg2.png)
