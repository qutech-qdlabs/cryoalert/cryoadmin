# Add Alert Rules and Collection

## Alert Rules

### Overview

![alert_rule_overview](../img/AlertRuleOverview.png)

### Create Alert Rule

To create a new alert rule, fill in the form as shown below. If the alert rule you wish to add contains no units, simply leave it empty.

![alert_rule_with_bounds](../img/AddAlertRuleWithBound.png)

## Alert Collections

### Overview

![alert_collection_overview](../img/AlertCollectionOverview.png)

### Create Alert Collection

To create a new alert collection, fill in the forms as shown below. From the list of alert rules available in the "Available rules" section, select the alert rules you want to add this this new collection and use the arrow to move those to the "Chosen rules" section. Then save to finalize the creation.

Note that, on its own, an alert collection doesn't do anything. It needs to be added to a fridge before CryoRelay can distribute the alert collection to the fridge's CryoAgent. See the [fridge documentation](new_fridge.md#adding-a-new-fridge) to learn how to do this.

![add_alert_collection](../img/AddAlertCollection.png)

### Add Alert Rule to Collection

To add an alert rule to an existing alert collection, click on the collection in the alert collection overview page, then select the alert rule you wish to add from the "Available rules" section and use the arrow to move it to the "Chosen rules" section. Then save to finalize the change.

If CryoRelay is setup, then it will distribute the alert collection change to the affected CryoAgents.

![add_alert_rule_to_collection](../img/AddAlertRuleToCollection.png)
