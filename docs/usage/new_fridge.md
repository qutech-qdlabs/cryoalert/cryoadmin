# Adding a new Fridge

## Overview

On the overview page you see a list of all fridges that have been added for your organization (or all organizations, if you are an admin). Click on the name of a fridge to see its details. Press the "add fridge" button in the top right to add a new fridge.

![fridge_overview](../img/FridgeOverview.png)

## Add new Fridge

To add a new fridge, fill in the form as shown below. It should be noted that the `url` parameter is the URL relative to the Prometheus agent installed on the RelayPC, i.e., it should be an adress that is reachable from the RelayPC. Additionally, it not required to fill in an `alert def` (Alert Collection). However, if you don't, then no alert rules will be send to the CryoAgent of this fridge. If it possible to add an alert collection later.

Furthermore, it is possible to exclude this fridge from certain parts of the CryoAlert platform by means of the three checkboxes;

- Enable service discovery: As the name implies, when this checkbox is checked, it will participate in the service discovery process of the Prometheus instance that will scrape the CryoAgent of this fridge. If this checkbox is unchecked, no data will be retrieved for this fridge.
- Enable alert updates: If this checkbox is unchecked, then this fridge will not receive any alert rule updates via CryoRelay.
- Enable cryogram request: If this checkbox is unchecked, then this fridge will not be subscribable via CryoAlertBot on Telegram.

![add_fridge](../img/AddFridge.png)
