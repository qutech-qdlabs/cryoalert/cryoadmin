# Adding a new API-key

## Overview

![api_key_overview](../img/APIKeyOverview.png)

## Add new API-key

To create a new API-key, fill in the form as shown below. It is not required to add an expiry date.

![add_api_key](../img/AddAPIKey.png)

After saving, the API-key is shown as a message in the browser (see below). This is shown only once and cannot be retrieved afterwards. Be sure to save it!

![new_api_key](../img/NewApiKey.png)
