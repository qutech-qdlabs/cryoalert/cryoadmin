# Production Deployment

CryoAdmin is deployed as a container, most likely in conjunction with other containers from the CryoAlert platform. It is therefore assumed you already have (or are working on) a docker-compose file from which CryoAdmin (and its database) can be started and updated.

## Chapters

- [Installation](installation.md)
- [Updating](updating.md)
