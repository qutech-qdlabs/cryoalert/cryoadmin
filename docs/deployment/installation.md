# Production Installation

## 1) Setup environment files

There is only one .env files that needs setting up; the `.env` file in the root directory needed to fill in environment variables for the `docker-compose.yml` file. A templates exists in the repository (`.env_template_prod`). Copy and rename it and fill in the variables as needed. Do note that, for production, you might want to consider running all dockerized applications from a single docker-compose.yml file. Because each deployment is unique, what to include will not be mentioned here.

## 2) Start Postgres database

```sh
> docker compose up -d postgres 
```

## 3) Setup CryoAdmin

```sh
> docker compose run -it cryoadmin -s
```

## 4) Start CryoAdmin

```sh
> docker compose up -d cryoadmin
```
