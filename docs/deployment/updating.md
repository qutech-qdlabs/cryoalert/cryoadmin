# Update CryoAdmin

To update CryoAdmin, simple change the version tag of the image used. Then run the commands below. This will automatically pull the selected version of the CryoAdmin image.

```sh
> docker compose stop cryoadmin
> docker compose up -d cryoadmin
```
