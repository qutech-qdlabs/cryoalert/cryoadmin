# CryoAdmin Documentation

The CryoAdmin application allows for central management of various settings used by multiple components of the CryoAlert Platform. Additionally, it contains endpoints for Prometheus' service discovery, retrieval of alert rule updates, and authentication/authorization for CryoGram.

## Chapters

- [Deployment](./deployment/index.md)
- [Usage](./usage/index.md)
- [Development](./development/index.md)
