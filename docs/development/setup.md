# Development Setup

## 1) Python Version

CryoAdmin currently only works with Python 3.11. 

## 2) Setup environment files and logging

There are two .env files that need setting up. The first is the `.env` file in the root directory needed to fill in environment variables for the `docker-compose.yml` file. 
The second is the `.backend.env` file in the `./cryo_admin/` directory. For both files templates exist in the repository. 
Copy, rename, and modify them as follows:
- copy `.env_template_dev` to `postgres.env` and modify the values there as needed
- in the `cryo_admin` directory, copy `.backend.env_template` to `.backend.env` and modify the values there as needed
- copy `.backend.env` from the `cryo_admin` directory to its parent directory (e.g. one directory up), and rename it `django.env`

Once this is done, also create a `logs` directory in the `cryo_admin` directory.

## 3) Start Postgres database

Using `docker compose`, start the postgres database.

```sh
docker compose up -d postgres 
```

## 4) Setup CryoAdmin

For CryoAdmin to work, some setup steps are required. Note that the `createadmin` command requests you to set a superuser username and password. Be sure to remember those!

```sh
cd ./cryo_admin
python manage.py migrate
python manage.py setup
python manage.py createadmin
```

## 5) Start CryoAdmin on machine

Finally, while still in the `./cryo_admin/` directory, start the development server

```sh
python manage.py runserver
```

## 6) Start CryoAdmin on Docker

Optionally, one can also build and start CryoAdmin as a container. This is the closest one can get to the production setup during development. For this, copy and fill the `.env_template_prod` file. Then,

```sh
docker compose up cryoadmin
```
