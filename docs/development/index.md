# Development Documentation

Before starting development on CryoAdmin, please read the general development [documentation](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/docs/-/blob/main/development/index.md) thoroughly. Afterwards, you can start on the development documentation specific to CryoAdmin that is documented here.

## Chapters

- [Setup](setup.md)
- [Build & Publish](build.md)
