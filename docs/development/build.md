# Build and Publish Documentation

CryoAdmin is deployed as a container. However, before it can be deployed, it must be build. For this, a helper (Windows batch) script is available. From the repository root directory, run the following command:

```bat
> .\.build-tools\publish\publish.bat
```

This will build the image and then push it to the TUDelft registry. You can store your image elsewhere if you so wish to. Note that, before you can push a new image, you must login to the registry using `docker login`. The required credentials can be acquired from a project member.
