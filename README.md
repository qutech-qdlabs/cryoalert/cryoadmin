# CryoAdmin

CryoAdmin is an application used to manage "all" settings related to cryoalert via a web interface. See the [documentation](./docs/index.md) for details.
